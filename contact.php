<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		.error{
			color: #00BFFF;
		}
	</style>
</head>
<body>
	<?php
	// define variables and set to empty values
	$nameErr = $phoneErr = $emailErr = $genderErr = $websiteErr = $commentErr= "";
	$name    = $phone = $email    = $gender    = $comment    = $website   = "";

	if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		if (empty($_POST["name"]))
		{
			$nameErr = "";
		}
		else
		{
			$name = test_input($_POST["name"]);
			// check if name only contains letters and whitespace
			if (!preg_match("/^[a-zA-z]*$/",$name))
			{
				$nameErr = "Only letters and whute space allowed";
			}
		}

		if (empty($_POST["phone"]))
		{
			$phoneErr = "";
		}
		else
		{
			$name = test_input($_POST["phone"]);
			// check if name only contains letters and whitespace
			if (!preg_match("/^[a-zA-z]*$/",$phone))
			{
				$phoneErr = "Only letters and whute space allowed";
			}
		}

		
		if (empty($_POST["email"]))
		{
			$emailErr = "";
		}
		else
		{
			$email = test_input($_POST["email"]);
			// check if e - mail address is well - formed
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			{
				$emailErr = "Invalid email format";
			}
		}

		if (empty($_POST["website"]))
		{
			$website = "Website is required";
		}
		else
		{
			$name = test_input($_POST["website"]);
			// check if name only contains letters and whitespace
			if (!preg_match("/^[a-zA-z]*$/",$website))
			{
				$websiteErr = "Only letters and whute space allowed";
			}
		}

		if (empty($_POST["comment"]))
		{
			$comment = "";
		}
		else
		{
			$comment = test_input($_POST["comment"]);
		}

	}

	function test_input($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	?>

<html lang="en" dir="ltr">
   <head>
      <meta charset="utf-8">
      <title>Kirim Email</title>
      <link rel="stylesheet" href="style.css">
   </head>
   <body>
      <div class="wrapper">
         <div class="title">
            Email
         </div>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
	<div class="field">
    <label>Nama : </label> &emsp;&emsp;&emsp;&emsp;&emsp;
            <input type="text" name="nama" value="<?php echo $name;?>">
            <span class="error">
			*   <?php echo $nameErr;?>
		        </span>
               
            </div>
            <br>

			<div class="field">
            <label>Number Phone : </label> &emsp;
            <input type="text" name="phone" value="<?php echo $phone;?>">
            <span class="error">
			*   <?php echo $phoneErr;?>
		        </span>
              
            </div>
            <br>
 
            <div class="field">
            <label>Email : </label> &emsp;&emsp;&emsp;&emsp;&emsp;
            <input type="text" name="email" value="<?php echo $email;?>">
            <span class="error">
			* <?php echo $emailErr;?>
		</span>
            
            </div>
            <br>

            <div class="field">
            <label>Website : </label> &emsp;&emsp;&emsp;&emsp;
            <input type="text" name="website" value="<?php echo $website;?>">
            <span class="error">
			*   <?php echo $websiteErr;?>
		        </span>
              
            </div>
            <br>

            <div class="field">
                <li>
                <label>Comment</label>
</li>
<li><textarea name="commet" rows="5" cols="40">
			<?php echo $comment;?>
</textarea>
            <span class="error">
			* <?php echo $commentErr;?>
		</span></li>
            
            
            
            </div>


            <div class="field">
               <input type="submit" value="Kirim" onclick="Kirim()">
            </div>

         </form>
		
	</form>

      </div>
   </body>
</html>