<!DOCTYPE html>
<html>
<head>
  <title>Data Riwayat Kepanitiaan</title>
  <style>
    body {
        background: #F0F8FF;
        margin: 0;
    }
    .container {
      max-width: 600px;
      margin: 10px 530px;
      padding: 20px;
      background-color:   #00BFFF;
      border-radius: 5px;
    }

    h1 {
      text-align: center;
    }

    .list {
      list-style-type: none;
      padding: 0;
      margin: 20px 0;
    }

    .list li {
      background-color: #F0F8FF;
      padding: 10px;
      margin-bottom: 10px;
      border-radius: 5px;
    }
  </style>
</head>
<body>
  <div class="container">
    <h1>Data Riwayat Panitia</h1>

    <ul class="list">
      <?php
      $mahasiswa = array(
        array("nama" => "Pekan Akhir Ramadhan Informatika", "divisi" => "Staf KWU", "waktu" => "2023"),
        array("nama" => "ICOM", "divisi" => "Staff Konsumsi", "waktu" => "2023"),
        array("nama" => "Next Gen Entrepreneurship Competition FTI x HMTI", "divisi" => "Staff Perkap", "waktu" => "2023")
      );

      for ($i = 0; $i < count($mahasiswa); $i++) {
        echo "<li>";
        echo "<h3>" . $mahasiswa[$i]['nama'] . "</h3>";
        echo "<p>Divisi: " . $mahasiswa[$i]['divisi'] . "</p>";
        echo  "<p>Tahun: ".$mahasiswa[$i]['waktu'] . "</p>";
        echo "</li>";
      }
      ?>
    </ul>
  </div>
</body>
</html>