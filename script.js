document.getElementById('loginForm').addEventListener('submit', function(e) {
    e.preventDefault(); // Prevent form submission
  
    // Get form values
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
  
    // Check if username and password are not empty
    if (username.trim() !== '' && password.trim() !== '') {
      // Perform login logic here
      // For demonstration purposes, we'll just show a success message
      document.getElementById('error').textContent = '';
      alert('Login successful!');
      window.location.href = "index.html";
    } else {
      // Show error message if fields are empty
      document.getElementById('error').textContent = 'Please enter username and password.';
    }
  });